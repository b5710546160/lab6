import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class WordCount {
	/**
	 * 
	 * @param args
	 *            ...
	 * @throws IOException
	 *             ...
	 */
	public static void main(String[] args) throws IOException {
		Map<String, Integer> world1 = new HashMap<String, Integer>();
		Count wonder = new Count(world1);
		String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt";
		URL url = new URL(FILE_URL);
		InputStream input = url.openStream();
		Scanner scanner = new Scanner(input);
		while (scanner.hasNext()) {
			final String DELIMS = "[\\s,.\\?!\"():;]+";
			scanner.useDelimiter(DELIMS);
			String alice = scanner.next();
			wonder.addWord(alice);
			// System.out.println(alice);
		}
		String[] a = wonder.getSortedWords();
		for (int i = 0; i < 20; i++) {
			System.out.println(a[i]+ " "+wonder.getCount(a[i]));
		}
		System.out.println("-------------------------------------------------");
		String[] b = wonder.getSortedValue();
		for (int i = 0; i < 20; i++) {
			System.out.println(b[i]+ " "+wonder.getCount(b[i]));
		}
	}

}
