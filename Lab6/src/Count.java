import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class Count {
	private Map<String, Integer> world = new HashMap<String, Integer>();
	/**
	 * 
	 * @param word Map added
	 */
	public Count(Map<String, Integer> word) {
		world = word;
	}
	/**
	 * 
	 * @param word what we want to add.
	 */
	public void addWord(String word) {
		String a = word.toLowerCase();

		if (world.containsKey(a)) {
			world.replace(a, world.get(a), world.get(a) + 1);
		} else {
			world.put(a, 1);
		}

	}
	/**
	 * 
	 * @return Set.
	 */
	public Set<String> getWords() {
		return world.keySet();

	}
/**
 * 
 * @param word what we search.
 * @return int count.
 */
	public int getCount(String word) {
		String a = word.toLowerCase();
		if (world.containsKey(a)) {
			return world.get(a);
		}
		return 0;
	}
	/**
	 * 
	 * @return result.
	 */
	public String[] getSortedWords() {
		
		Set<String> bb = getWords();
		String[] result = new String[bb.size()];
		bb.toArray(result);
		Arrays.sort(result);
		return result;
		
	}
	/**
	 * 
	 * @return result of sort.
	 */
	public String[] getSortedValue() {
		ValueComparator bvc =  new ValueComparator(world);
        Map<String,Integer> sortedmap = new TreeMap<String,Integer>(bvc);
        sortedmap.putAll(world);
        Set<String> bb =sortedmap.keySet();
        String[] result = new String[bb.size()];
		bb.toArray(result);
		return result;
	}
	/*public static void main(String [] args) throws IOException {
		Map<String, Integer> world1 = new HashMap<String, Integer>();
		world1.put("apple", 1);
		world1.put("papaya", 1);
		Count s = new Count(world1);
		s.addWord("apple");
		System.out.print(s.getCount("apple"));
		
	}*/

}
